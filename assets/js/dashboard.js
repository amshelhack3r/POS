//create a class section in which each individual section will inherit from
import {
    Utils,
    UserException,
    Server,
    LocalStorage
} from './util.js';
class Section {
    constructor() {
        //get the id of the section
        this.id = this.constructor.name
        this.section = document.querySelector(`#${this.id.toLowerCase()}`)
        this.init();
        this.loadElements(this.section);
        this.addListeners();
        this.search(document.querySelector("#search-box"));
    }

    init() {
        document.querySelector('section.active').classList.remove('active')
        this.section.classList.add('active')
        // const navBtn = document.querySelector('.hide-nav');
        // navBtn.addEventListener('click', () => {
        //     navBtn.parentNode.parentNode.style.display = "none";
        // });

    }

    loadElements() {
        throw new UserException(`Must implement ${this.loadElements.name} in child classes`)
    }

    addListeners() {
        throw new UserException(`Must implement ${this.addListeners.name} in child classes`)
    }

    search(elem) {
        //dont show the search bar if child clases havent implemented the method
        document.querySelector('#search-box').parentNode.style.display = 'none';

        return false;
    }

    static switch (target, data) {
        data = (typeof data !== undefined) ? JSON.stringify(data) : undefined
        // document.querySelector('section.active').classList.remove('active')
        // document.querySelector(`#${target}`).classList.add('active')


        eval(`new ${target}(${data})`)
    }


}

class Dashboard extends Section {
    constructor() {
        super()
    }

    loadElements(section) {
        this.cards = section.querySelectorAll('.card')
    }

    addListeners() {
        this.cards.forEach(card => {
            if (card.hasAttribute('target')) {
                card.classList.add('clickable-card')
                card.addEventListener('click', () => {
                    const section = card.getAttribute('target')
                    //evaluate the clas
                    eval(`new ${Utils.toTitleCase(section)}`)
                })
            }
        })

    }


}

class Record extends Section {
    constructor() {
        super()
    }

    loadElements(section) {
        this.sessionCards = section.querySelector("#session-cards");
    }

    addListeners() {
        //load all session details as soon as the section loads
        (() => {
            const staffData = LocalStorage.getFromStorage('staff');
            const stationData = LocalStorage.getFromStorage('station');

            const postData = {
                class: 'StockSession',
                method: 'getSessionList',
                state: true
            }

            Server.post(postData, (response) => {
                console.log(response);
                response.data.forEach(item => {
                    const staff = staffData.filter(member => member.staff == item.staff)[0].name;
                    const station = stationData.filter(member => member.station == item.station)[0].name;
                    const formattedDate = moment(item.date).format("dddd, MMMM Do YYYY, h:mm a")
                    let elem = this.buildCard(item.session, formattedDate, staff, station, item.direction)

                    this.sessionCards.appendChild(elem)

                })
                this.buttons = this.section.querySelectorAll('.session-details');
                this.buttonListener()

            })
        })();

    }

    buttonListener() {
        this.buttons.forEach(button => {
            button.addEventListener('click', () => {
                //get the session id
                const id = button.parentNode.getAttribute('id');

                const postData = {
                    class: "StockSession",
                    method: "getSessionDetails",
                    state: true,
                    data: {
                        id: id.split('_')[1]
                    }
                }
                Server.post(postData, (response) => {
                    console.log(response);

                    Section.switch('Table', response.data);

                })

            })
        })
    }
    buildCard(id, date, staff, station, direction) {
        let card = document.createElement('div');
        card.classList.add('card');

        const html = `<div class="card-body records">
        <h6  class="text-dark text-center">Date: ${date}</h6>
        <div class="row d-flex justify-content-around" id="session_${id}">
            <p class="lead session-staff">Staff: <span class="text-dark">${staff}</span></p>
            <p class="lead session-counter">Counter: <span class="text-dark">${station}</span></p>
            <p class="lead session-direction">Direction: <span class="text-dark">${direction}</span></p>
            <button class="btn btn-icon icon-left btn-info session-details" id=""><i class="fas fa-info-circle"></i>VIEW</button>
        </div>
    </div>`;
        card.innerHTML = html;
        return card;
    }


}

class Stock extends Section {
    constructor() {
        super();
        this.deleteLsData();
        //create a copy of products and save to ls to mointor data collected
        LocalStorage.saveToStorage('temp-data', LocalStorage.getFromStorage('products'));
        //update values
        this.updateVals();
        //save the array to memeory for easy manipulation
        this.data = LocalStorage.getFromStorage('temp-data');
        //set the current product for easier massaging
        this.currentProduct = this.data[0];
        //set the first item to be read
        this.addProductItem(this.currentProduct);


    }

    loadElements(section) {
        //load the searchbar
        this.searchBar = document.querySelector('#search-box');

        //remainder for stock left for reading
        this.remainder = section.querySelector("#remainder");
        //initialise the table
        this.table = section.querySelector('#product-table');
        //temp body to append stock
        this.tBody = section.querySelector("#temp-body");
        //image element for pproduct
        this.image = section.querySelector("#pombe-image");
        //form to submit stock
        this.form = section.querySelector('#submit-drink');
        //set product name
        this.productName = section.querySelector("#product-name");
        //barcode for item
        this.productBarcode = section.querySelector("#product-barcode");
        //input for quantity
        this.quantity = section.querySelector('#qty');

        //buttons for sending to or from server
        this.upload = section.querySelector('#upload');
        this.clear = section.querySelector('#clear');

        //this is the session form info. hide this after registering a session
        this.sessionCard = section.querySelector("#session-card");
        //this is the cardfor data entry. unhide this after entering session
        this.entry = section.querySelector(".stock-entry");

        //form to supply session info
        this.sessionForm = section.querySelector("#session-form");

        //span details
        this.staffSpan = section.querySelector('#staff-detail');
        this.stationSpan = section.querySelector('#station-detail');
        this.dateSpan = section.querySelector('#date-detail');
        this.directionSpan = section.querySelector('#direction-detail');
        this.statusSpan = section.querySelector('#status-detail');


    }

    addListeners() {
        //function for populating dropdowns as soon as they are initialised from the database
        (() => {
            let _this = this;

            //initialise products for datatabe
            $(document).ready(function () {
                const table = $('#product-table').DataTable({
                    scrollY: 400,
                    retrieve: true,
                    destroy: true,
                    "data": LocalStorage.getFromStorage("products"),
                    "columns": [{
                        "data": "name"
                    }]
                });

                $('#product-table tbody').on('click', 'tr', function () {
                    _this.currentProduct = table.row(this).data();
                    _this.addProductItem(_this.currentProduct);
                });
            });
            const staffData = LocalStorage.getFromStorage('staff')
            const stationData = LocalStorage.getFromStorage('station')
            Server.post({
                class: 'Chicjoint',
                method: 'getSessionVariables',
                state: true
            }, (response) => {
                const staff = staffData.filter(staff => staff.staff == response.data.staff)[0];
                const station = stationData.filter(station => station.station == response.data.station)[0];

                this.staffSpan.innerText = staff.name;
                this.stationSpan.innerText = station.name;
            })

        })();

        //add a listener when the user tries  to submit a form
        this.sessionForm.addEventListener('submit', (e) => {
            e.preventDefault();

            //create a form data object using the form
            const formData = new FormData(this.sessionForm);

            const date = moment(formData.get("session-date")).format("YYYY-MM-DD h:mm:ss");
            const direction = formData.get("session-direction");
            const status = formData.get("session-status");

            const details = {
                date,
                direction,
                status
            };
            // //change the span texts
            // this.staffSpan.innerText = staff;
            // this.stationSpan.innerText = station;
            this.dateSpan.innerText = date;
            this.directionSpan.innerText = direction;
            this.statusSpan.innerText = status;

            LocalStorage.saveToStorage('session-details', details);

            this.sessionCard.classList.add("no-display");
            this.entry.style.display = 'block';
        });

        this.form.addEventListener('submit', (e) => {
            e.preventDefault();
            const tr = document.createElement('tr');
            const tableTd = document.createElement('td');
            tableTd.innerText = this.currentProduct.name;
            const quantityTd = document.createElement('td');
            quantityTd.innerText = this.quantity.value;

            tr.appendChild(tableTd);
            tr.appendChild(quantityTd);
            this.tBody.appendChild(tr);

            //get table from local LocalStorage
            let tableLs = LocalStorage.getFromStorage('temp-table');
            const array_id = this.data.filter(item => item.name === this.productName.innerHTML)[0];

            tableLs.push({
                product: array_id.product,
                quantity: this.quantity.value,
            });
            LocalStorage.saveToStorage('temp-table', tableLs);
            if (this.data.length > 1) {
                this.data = this.data.filter(item => parseInt(item.product) !== parseInt(this.currentProduct.product));
                this.currentProduct = this.data[0];

                this.addProductItem(this.currentProduct);
                LocalStorage.saveToStorage('temp-data', this.data);
                this.updateVals();
            } else {
                //completed data entry show a modal for notifying the user
                alert("finished data entry");
            }


        });

        //this are listeners when the user tries to upload the table
        this.upload.addEventListener('click', () => {
            //set a prompt to ask the user if he/she wants to commit to db
            swal({
                title: "Save Session?",
                text: "Save the current stock taking session?",
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then(ok => {
                if (ok) {
                    const session = LocalStorage.getFromStorage('session-details');

                    const postData = {
                        class: "StockSession",
                        method: "insertSessionData",
                        state: false,
                        data: {
                            session,
                            table: LocalStorage.getFromStorage('temp-table'),
                            staff: session.staff,
                            station: session.station,
                            date: session.date
                        }
                    };
                    Server.post(postData, (data) => {
                        console.log(data);

                        swal({
                            title: "NICE!",
                            text: "Successfully uploaded",
                            icon: "success"
                        });

                        this.updateVals();
                        this.clearTable();
                        Section.switch('Record');
                    });
                } else {
                    swal("Ok. Continue 😀");
                }
            })
        });
        this.clear.addEventListener('click', () => {
            swal({
                    title: "DELETE TABLE DATA?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((del) => {
                    if (del) {
                        LocalStorage.removeFromStorage('table');
                        this.updateVals();
                        this.clearTable();
                        swal("Table deleted", {
                            icon: "success",
                        });
                    } else {
                        swal("Ok. Continue 😀");
                    }
                });
        })

    }

    search(elem) {
        elem.parentNode.style.display = 'block';
        elem.removeEventListener('click', () => {});
        elem.addEventListener('change', (e) => {

            console.log(e.target.value);

            //check if the user has registered a session
            if (this.sessionCard.classList.contains("no-display")) {
                this.data.forEach(item => {
                    if (e.target.value !== "") {
                        if (item.barcode === e.target.value) {
                            console.log(item);
                            this.currentProduct = item;
                            this.addProductItem(this.currentProduct);
                            e.target.value = '';
                            return;
                        }
                    }
                })
            } else {
                swal("You must register a session to use search");
            }
            //check if there exists a product with that barcode. in temp directory
            e.preventDefault();
        });


    }

    updateVals() {
        const data = LocalStorage.getFromStorage('temp-data');
        this.remainder.innerText = `${data.length} Items remaining`;

        const table = LocalStorage.getFromStorage('temp-table');
        if (table.length > 0) {
            this.upload.style.display = 'block';
            this.clear.style.display = 'block';
        } else {
            this.upload.style.display = 'none';
            this.clear.style.display = 'none';
        }


        this.quantity.value = "";
        this.quantity.focus()
    }

    deleteLsData() {
        LocalStorage.removeFromStorage('data');
        LocalStorage.removeFromStorage('temp-table');
    }

    addProductItem(item) {
        this.productName.innerText = item.name;
        this.productBarcode.innerText = item.barcode;
        this.image.setAttribute('src', `../assets/images/${item.image}`);
    }

    clearTable() {
        this.tBody.innerHTML = ""
    }
}

class Setting extends Section {
    loadElements(section) {
        this.cards = section.querySelectorAll('.card')
    }

    addListeners() {
        this.cards.forEach(card => {
            if (card.hasAttribute('target')) {
                card.classList.add('clickable-card')
                card.addEventListener('click', () => {
                    const section = card.getAttribute('target')
                    //evaluate the clas
                    eval(`new ${Utils.toTitleCase(section)}`)
                })
            }
        })
    }

    // search() {
    // }
}

class Product extends Section {
    constructor() {
        super()


    }

    loadElements(section) {
        const products = LocalStorage.getFromStorage('products')
        const dataArray = Array.from(products, item => [item.name])

        const productName = section.querySelector('#Pname')
        const productBarcode = section.querySelector('#Pbarcode')
        const ProductPrice = section.querySelector('#Pprice')
        const productPicture = section.querySelector('#Pimage')
        $(document).ready(function () {
            const table = $('#product-table').DataTable({
                scrollY: 400,
                paging: false,
                "data": dataArray

            });

            $('#product-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                // alert( 'You clicked on '+data[0]+'\'s row' );

                $.each(products, function (index, value) {
                    if (value.name === data[0]) {
                        productName.value = value.name
                        productBarcode.value = value.barcode
                        ProductPrice.value = value.amount
                        productPicture.setAttribute('src', `../assets/images/${value.image}`)
                    }
                })
            });

        });
        //pproduct rows

        // this.ProductPrice = section.querySelector('#')


    }

    addListeners() {


    }


}

class Report extends Section {
    constructor() {
        super();
    }
    loadElements(section) {
        this.cards = section.querySelectorAll('.card');
        this.form = section.querySelector('#report-form');
    }
    addListeners() {
        this.form.addEventListener('submit', (e) => {
            const formData = new FormData(this.form);
            const date = formData.get('date')
            
            const postData = {
                class: "Reports",
                method: this.type,
                state: false,
                date
            }
            Server.post(postData, (response) => {
                console.log(response);
            })
            e.preventDefault();
        })
        this.cards.forEach(card => {
            card.classList.add('clickable-card');
            card.addEventListener('click', () => {
                this.type = card.getAttribute('method');
                $('#modal-selector').modal({
                    backdrop: false
                });
            });
        });
    }
}

class Table extends Section {

    constructor(data) {
        super()
        this.data = data;
        this.loadData()
    }

    loadElements(section) {
        this.staff = section.querySelector('#staff-detail');
        this.station = section.querySelector('#station-detail');
        this.date = section.querySelector('#date-detail');
        this.direction = section.querySelector('#direction-detail');
        this.tbody = section.querySelector('#temp-body');
    }

    loadData() {
        // console.log(data);
        const session = this.data.session_details;
        const staffData = LocalStorage.getFromStorage('staff');
        const stationData = LocalStorage.getFromStorage('station');
        const staff = staffData.filter(member => member.staff == session.staff)[0].name;
        const station = stationData.filter(member => member.station == session.station)[0].name;
        const formattedDate = moment(session.date).format("dddd, MMMM Do YYYY, h:mm a")

        this.staff.innerText = staff
        this.station.innerText = station
        this.date.innerText = formattedDate
        this.direction.innerText = this.data.session_details.direction;

        this.buildTable(this.data.table)
    }
    addListeners() {}

    buildTable(rows) {
        rows.forEach(row => {
            let tr = document.createElement('tr');
            let productTd = document.createElement('td');
            let quantityTd = document.createElement('td');
            productTd.innerText = row.name;
            quantityTd.innerText = row.value;

            tr.appendChild(productTd)
            tr.appendChild(quantityTd)
            this.tbody.appendChild(tr);
        })
    }

}
/**
 * class ErrorPage this is a class/page that will handle all server errors and display a static error page
 */
class ErrorPage extends Section {
    constructor(code, body) {
        super();
        console.log(code);

        this.statusCode.innerHTML = code;
        this.statusMessage.innerText = body;
    }

    loadElements(section) {
        this.statusCode = section.querySelector('#status-code');
        this.statusMessage = section.querySelector('#status-message');
    }
    addListeners() {}
}

export {
    Dashboard,
    Record,
    Stock,
    Setting,
    Product,
    Report,
    Table,
    ErrorPage
}