/**
 * @classdesc create a class that handles my own exceptions.
 * Use the sweet alert library to display my exception to the user
 */
class UserException {
    /**
     * 
     * @param {string} message This will be message being shown by sweet alert
     */
    constructor(message) {
        this.message = message
        this.name = 'UserException'

        swal({
            title: "Exception!!",
            text: message,
            icon: "warning",
            dangerMode: true,
        })
    }

}
/**
 * @classdesc This class will hold helper methods i.e regex conversions or string conversions
 * all methods should be static in nature
 * 
 */
class Utils {
    /**
     * @method Converts a STRING to sentence case i.e foo->Foo, bar->Bar
     * @param {string} word 
     * @returns string
     */
    static toTitleCase(word) {
        return word.replace(
            /\w\S*/g,
            function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }
}

/**
 * @classdesc This class will be used for manipulating data from localStorage
 * 
 */
class Storage {
    /**
     * @property storage This will be overriden to set if its localStorage or sessionStorage
     */
    static storage;

    /**
     * @method saveToStorage save Stringified value to LocalStorage
     * @param {string} key 
     * @param {any} value 
     */
    static saveToStorage(key, value) {
        this.storage.setItem(key, value)
    }

    /**
     * @method getFromStorage get a Parsed value from localStorage
     * @param {string} id 
     */
    static getFromStorage(id) {
        return this.storage.getItem(id)
    }
    
    /**
     * 
     * @method Provided a key, remove the value if it exists in local Storage
     * @param {string} key 
     */
    
    static removeFromStorage(id){
        this.storage.removeItem(id)
    }
    
    static clear() {
        this.storage.clear()
    }
}

/**
 * @class LocalStorage
 * @classdesc Class for handling all localStorage operations
 */
class LocalStorage extends Storage{
    static storage = window.localStorage;

     /**
     * @method saveToLs save Stringified value to LocalStorage
     * @param {string} key 
     * @param {any} value 
     */
    static saveToStorage(key, value) {
        this.storage.setItem(key, JSON.stringify(value))
    }

    /**
     * @method getFromStorage get a Parsed value from localStorage
     * @param {string} id 
     */
    static getFromStorage(id) {
        const value = this.storage.getItem(id)
        if (value !== null) {
            return JSON.parse(this.storage.getItem(id))
        } else {
            this.saveToStorage(id, [])
            return this.getFromStorage(id)
        }
    }

    /**
     * @method removeFromLs if parsed value is an array, remove oa item then save the remainder
     * @param {string} key 
     * @param {any} id 
     */
    static removeSpecific(key, id) {
        const data = this.getFromStorage(key)
        const new_data = data.filter(value => value.primary !== id)
        this.saveToStorage(key, new_data)
    }
}

class SessionStorage extends Storage{
    static storage = window.sessionStorage;
}

/**
 * @class Server
 * @classdesc Class for handling server operations i.e get and post
 */
class Server {
    static url = `${document.location.origin}/POS/lib/Chicjoint.php`;
    

    /**
     * 
     * @param {Function} callback 
     */
    static async get(callback) {
        const response = await fetch(this.url)
        if (await response.status === 200) {
            callback(await response.json())
        } else {
            console.error(`Server came bak with status ${await response.status} body: ${await response.body}`);

        }
    }
    /**
     * @method Sends a post request to {chicjoint.php}, returns a promise which is handled by callback function
     * @param {JSON} data data to be sent to the server.
     * @param {Function} callback This is called if the we get a positive response
     * @returns promise
     */
    static async post(data, callback) {
        console.log(this.url);
        
        const response = await fetch(this.url, {
            method: 'post',
            body: JSON.stringify(data)
        })
        callback(await response.json());
    }

}

export {UserException, Utils, LocalStorage, SessionStorage, Server};