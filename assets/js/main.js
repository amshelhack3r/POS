import {
    LocalStorage,
    SessionStorage,
    Server,
    Utils
} from './util.js';
import * as Section from './dashboard.js';

//speed up debbuging, declare a preset section
var activeSection = "Dashboard";
//create the main entrypoint for our system
const POS = (() => {
    const showClock = () => {
        const time = moment().format('MMMM Do YYYY, h:mm:ss a')
        document.querySelector('.time').innerHTML = time
        setTimeout(showClock, 1000)
    };

    const updateLoggedInTime = () => {
        const time = moment(LocalStorage.getFromStorage('loggedIn'), 'hmmss').fromNow()
        document.querySelector('.logged-in').innerHTML = `LOGGED ${time}`
        setTimeout(updateLoggedInTime, 60000)
    };


    const logout = () => {
        swal({
                title: "LOGOUT?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((logout) => {
                if (logout) {
                    LocalStorage.removeFromStorage('loggedIn');
                    swal("You have successfully logged out", {
                        icon: "success",
                    });
                    Server.post({
                        class: 'Chicjoint',
                        method: 'deleteSessionVariables',
                        state: true
                    }, (response) => {
                        console.log(response);
                        window.location.href = "../index.php"
                    })


                } else {
                    swal("Logout cancelled");
                }
            });
    }
    return {
        start: () => {

            LocalStorage.saveToStorage('loggedIn', moment().format('hmmss'))
            showClock()
            updateLoggedInTime()

            iziToast.success({
                title: 'Logged In',
                message: 'Welcome back Emma',
                position: 'bottomCenter'
            });

            //set listener to the logout button
            const logoutBtn = document.querySelector('#logout-btn');
            logoutBtn.addEventListener('click', logout);

            const list_navbar = document.querySelector('.main-nav')
            const list_items = list_navbar.querySelectorAll('li')

            list_items.forEach(item => {
                if (item.hasAttribute('target')) {
                    item.addEventListener('click', () => {
                        //set the list item as active
                        list_navbar.querySelector('.active').classList.remove('active')
                        item.classList.add('active')


                        const section = item.getAttribute('target')
                        //evaluate the clas
                        eval(`new Section.${Utils.toTitleCase(section)}`)
                    })
                }
            });


            eval(`new Section.${Utils.toTitleCase(activeSection)}`);
        },

    }
})();

//start the system 
POS.start();