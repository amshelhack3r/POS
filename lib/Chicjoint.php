<?php

/**
 * @todo insert session data gotten from javascript
 * @todo retrieve list of sessions created throughout the lifecycle
 * @todo given a session id retrieve relevant session data associated with it
 * @todo write a query that retirves the last closing balance for a particular product
 * 
 */
require __DIR__ . '/BaseModel.php';



if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}



/**
 * Since the point of sale heavily relies on ajax requests we get the post requests from javascript 
 * The javascript request needs to have atleast 3parameters-:
 * Class: This represents the class that will be instatiated
 * Method: this represents the method that will be invoked
 * State: Whether the method is static or not
 */
$data = json_decode(file_get_contents('php://input'));

//check for class and method variables
if (isset($data->class) && isset($data->method)) :
    run($data->class, $data->method, $data->state);
endif;

//create a  function that takes in a class name and method and runs the class and method
function run($class, $method, $static = false)
{
    if (class_exists($class)) :
        //check if method exists
        if ($static) :
            return (method_exists($class, $method)) ? $class::$method() : die("Class $class doesnt contain method $method");
        else :
            $obj = new $class;
            return (method_exists($obj, $method)) ? $obj->$method() : die("Class $class doesnt contain method $method");
        endif;
    else :
        die("No class by the name $class");
    endif;
}



/**
 * This will be the main php entry file
 * Most database operations will be handled by this file
 */
class ChicJoint
{
    /**
     * @method init
     * This method will be used to pull out system wide values that wont change when the system is operational
     * i.e products, staff, and stations
     * We get this info and save them to javascri local storage and this will atleast speed up call that i have to make 
     * to the server.  
     */
    public function init()
    {
        //create an empty array to store the result
        $arr = [];

        //fetch the products, staff and stations and save them in an associative array
        $arr['products'] = ModelController::getModelRecords('product');
        $arr['staff'] = ModelController::getModelRecords('staff');
        $arr['station'] = ModelController::getModelRecords('station');
        $arr['category'] = ModelController::getModelRecords('category');
        //return data to the client

        $response = new Ajax(200, $arr);
        echo $response;
    }

    /**
     * @method void setSessionVariables()
     * 
     */
    public static function setSessionVariables()
    {
        global $data;
        foreach ($data->data as $key => $value) :
            $_SESSION[$key] = $value;
        endforeach;
        echo new Ajax(200);
    }

    public static function deleteSessionVariables()
    {
        session_destroy();
        echo new Ajax(204);
    }

    public static function getSessionVariables()
    {
        if (count($_SESSION) > 0) :
            echo new Ajax(200, $_SESSION);
        else :
            echo new Ajax(400, null, "NO SESSION DATA");
        endif;
    }
}

/**
 * Class Session
 * This class will be for controlling session details, either inserting a session or retrieving pprevious sessions
 */
class StockSession
{
    /**
     * @method insertSessionData 
     * This method will be used to insert data taken from a session
     * It will conprise of two parts
     * The first will be to insert the session details. i.e staff, station, date and direction
     * This will help us get the session primary key which is essential in inserting the quantities
     * The next part willl be inserting the quantites, we get them from the table posted from the client
     */
    public function insertSessionData()
    {
        //get post items
        global $data;
        //insert session details
        $session_details = $data->data;

        $sessionData = new stdClass;
        $sessionData->date = $session_details->session->date;
        $sessionData->direction = $session_details->session->direction;
        $sessionData->staff = $_SESSION['staff'];
        $sessionData->station = $_SESSION['station'];

        $session = ModelController::createModel('session', $sessionData);
        $quantityData = new stdClass;
        $quantityData->session = $session->session;
        $quantityData->data = $session_details->table;
        //insert quantities
        ModelController::createModel('quantity', $quantityData);

        echo new Ajax(201, [$session]);
    }
    /**
     * @method getSessionList 
     * This method will be used to get all sessions in the database regardless of date direction staff or station
     * This will be displayed in a page called sessions where the user can query the particulars of a session */
    public static function getSessionList()
    {
        $session = ModelController::getModelRecords('session');
        echo new Ajax(200, $session);
    }

    /**
     * @method getSessionDetails
     * Given a session id, get the particulars i.e quantities products, staff, stations for that particuular session
     */
    public static function getSessionDetails()
    {
        global $data;
        $id = $data->data->id;
        $session = ModelController::getSingleRecord('session', ['session' => $id]);
        $quantity = new Quantity;
        $table = $quantity->getItems($session->session);

        //create an std class for returning the data to javascropt
        $response = [
            'session_details' => $session,
            'table' => $table
        ];

        echo new Ajax(200, $response);
    }
}

/**
 * class reports 
 * This class will be associated in retrieving all reports requested by the system
 */
class Reports
{
    public string $date;
    public function __construct()
    {
        global $data;
        $this->date = $data->date;
    }
    public function salesReport()
    {
    }
    //get report for all stock taken at a particular day and station
    public function stockReport()
    {
        /**
         * Stocks report is a report of stock taken at bhat particular day vs last stock taken on previous date
         * We get the last closing stock, calculate the new stock
         * We also calculate the added stock for that date to get the full calculated stock
         * Descrepancices are also calculated here
         *  
         */
        $sessions = Session::getDateSessions($this->date);

        //check if there are sessions
        if (count($sessions) > 0) :
            //final array 
            $final_array = array();
            //date has multiple sessions
            foreach ($sessions as $session) :
                $quantity = new Quantity;
                $arr = array(
                    'date' => $session->date,
                    'staff' => $session->staff,
                    'items' => $quantity->getItems($session->session)
                );
                array_push($final_array, $arr);
            endforeach;

            //echo the data
            echo new Ajax(200, $final_array);
        else :
            //no content to return 
            echo new Ajax(204, []);
        endif;
    }
}

/**
 * @class BarcodeController
 * 
 */
//this code is for inserting barcode readings
class BarcodeController
{
    public $db;
    public $barcode;
    public $data;

    public function __construct()
    {
        $this->db = Database::getInstance();
        if (sizeof($_POST) > 0) {
            if (isset($_POST['type'])) {
                $this->update_quantity();
            } else {
                //insert new record
                $this->insertNewBarcode();
            }
        } else {
            //scan a barcode
            $postData = file_get_contents('php://input');
            $this->data = json_decode($postData);
            $this->barcode = $this->data->barcode;
            $this->checkBarcodeInDb();
        }
    }


    function checkBarcodeInDb()
    {
        $sql = "SELECT barcode FROM product WHERE barcode = '$this->barcode' ";
        $result = $this->db->query($sql);

        if ($result->rowCount() == 0) {
            http_response_code(404);
            return;
        } elseif ($result->rowCount() > 0) {
            $this->incrementCount();
            http_response_code(202);
            return;
        } else {
            http_response_code(500);
            die("Invalid output");
        }
    }

    function incrementCount()
    {
        $sql = "SELECT quantity FROM product WHERE barcode = '$this->barcode'";
        $result = $this->db->query($sql);
        $quantity = $result->fetchObject()->quantity;
        $quantity++;
        $update = "UPDATE product SET quantity = $quantity WHERE barcode = '$this->barcode'";
        $this->db->exec($update);
    }

    function update_quantity()
    {
        $barcode = $_POST['scanned'];
        $sql = "select quantity from product where barcode = '$barcode'";
        $result = $this->db->query($sql);
        $quantity = $result->fetchobject()->quantity;
        $quantity += $_POST['quantity'];
        $update = "update product set quantity = $quantity where barcode = '$barcode'";
        $this->db->exec($update);
        http_response_code(202);
    }

    function insertNewBarcode()
    {
        $product_name = $_POST['name'];
        $this->barcode = $_POST['unique'];
        $quantity = 0;

        if (isset($_POST['quantity'])) {
            $quantity = intval($_POST['quantity']);
        }
        $amount = $_POST['amount'];


        $sql = "INSERT INTO product
                    (name, barcode, quantity, amount, client) 
                VALUES
                    ('$product_name', '$this->barcode', $quantity, '$amount', 1)";

        $this->db->exec($sql);

        http_response_code(201);
    }
}
