<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="./assets/css/stisla.css">
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <h3>CHIC JOINT POS</h3>
                        </div>

                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Login</h4>
                            </div>

                            <div class="card-body">
                                <form id="auth-form" action="post">
                                    <div class="form-group">
                                        <label for="staff">Enter Staff</label>
                                        <select class="form-control" id="staff" name="staff-dropdown" required>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="station">Enter Counter</label>
                                        <select class="form-control" id="station" name="station-dropdown" required>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4" value="login" />
                                </form>
                            </div>
                        </div>

                        <div class="simple-footer">
                            Copyright &copy; MUTALL 2020
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="assets/js/lib/sweetalert.min.js"></script>
    <script type="module">

        import {SessionStorage, Server, LocalStorage} from './assets/js/util.js';
        const postData = {
            class: 'Chicjoint',
            method: 'init',
            state: false
        };
        Server.post(postData, (data)=>{
            console.log(data);
            
            for (let [key, value] of Object.entries(data.data)) {
                LocalStorage.saveToStorage(key, value)
            }

                    //get the two drop downs
            const stationDropdown = document.querySelector('#station');
            const staffDropdown = document.querySelector('#staff');

            const stationData = LocalStorage.getFromStorage('station');
            const staffData = LocalStorage.getFromStorage('staff');
            


            stationData.forEach(station => {
                let option = document.createElement('option');
                option.setAttribute('value', station.station);
                option.innerText = station.name;
                stationDropdown.appendChild(option);
            });

            staffData.forEach(staff => {
                let option = document.createElement('option');
                option.setAttribute('value', staff.staff);
                option.innerText = staff.name;
                staffDropdown.appendChild(option);
            });

        });
        const authForm = document.querySelector('#auth-form');

        authForm.addEventListener('submit', (e)=>{
            e.preventDefault()
            //do some authentication
            const formData = new FormData(authForm);
            Server.post({
                class:'Chicjoint',
                method:'setSessionVariables',
                state:true,
                data:{
                    staff:formData.get('staff-dropdown'),
                    station:formData.get('station-dropdown'),
                }
            }, (data)=>{
                console.log(data);
            });
           
            swal({
                title: "Good job!",
                text: "Successfully logged in",
                icon: "success"
            });
            setTimeout(() => {
                window.location.href = "pages/dashboard.php"
            }, 2000)
        });
       
        const login = () => {
            

        }
    </script>
</body>

</html>